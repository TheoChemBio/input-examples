#!/bin/bash 
#SBATCH --nodes=1
#SBATCH -t 00:30:00
#SBATCH -A 2018-1-45 
#SBATCH -J Q-CHEM
#SBATCH -o stdout.txt
#SBATCH -e stderr.txt

# Job name 
JOB=h2o

# Set number of threads for OpenMP (-nt) or MPI processes (-np) for parallel run
# NOTE: hybrid OpenMP+MPI not supported. More info: http://www.q-chem.com/qchem-website/manual/qchem51_manual/sec-parallel.html
nthreads=32
#nproc=32

# load compilers and libraries
module load intel/18.0.0.128

# MPI
export QCRSH=ssh
export QCMPI=craympi
echo $SLURM_NODELIST

# set path to qchem executables
export QC=/cfs/klemming/nobackup/i/iubr/qchem_exe

# set QCAUX
export QCAUX=/cfs/klemming/nobackup/i/iubr/qcaux

# set scratch directory
export QCSCRATCH=$SNIC_TMP

# access shared libraries
export CRAY_ROOTFS=DSL

# run qchem script
source $QC/bin/qchem.setup.sh

# load compiler libraries
source /pdc/vol/intel/18.0.0.128/compilers_and_libraries_2018.0.128/linux/bin/compilervars.sh intel64

# run qchem  
qchem -nt $nthreads $JOB.in $JOB.out

