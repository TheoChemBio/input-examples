#!/usr/bin/perl

   use strict;
   use warnings;
   use Math::Trig;

   my ($file, $frame, $hfc_found, $i, $j, $k, $aii, $aji);
   my (@hfcc, @pa, @new_hfc, @rot_25);

#  get filenames
   my @outfiles=`ls *.hfc.out`;
   my $nFiles=@outfiles;
   my $nFrames=@outfiles;
   for $file ( 1 .. @outfiles )
   {
      chomp($outfiles[$file-1]);
      $outfiles[$file-1] =~ s/W_//;
      $outfiles[$file-1] =~ s/\.qmmm\.hfc\.out//;
   }

   for $i ( 0 .. 2 )
   {
      $hfcc[$i] = 0.0;
   }

   for $frame ( 1 .. $nFiles )
   {
#      $frame = $outfiles[$file-1];

#     read iso-HFCC from output
      $hfc_found = 0;
      open FL,"W_$frame.qmmm.hfc.out"
         or die "Cannot open file W_$frame.qmmm.hfc.out !\n";
      while ( <FL> )
      {
         if ( /Fermi contact contribution to HFC/ )
         {
            while ( <FL> )
            {
               if ( /\| 66N  \|   14  \|/ )
               {
                  $hfc_found = 1;
                  s/\|/ /g;
                  @_  = (split);
printf "%d  %.3f  %.3f  %.3f\n", $frame, $_[2], $_[3], $_[4];
                  $hfcc[0] += $_[2];
                  $hfcc[1] += $_[3];
                  $hfcc[2] += $_[4];
               }
               last if ( /Spin-dipolar contribution to HFC/ );
            }
         }
      }
      close FL;
      die "HFC tensor not found in frame $frame !\n" 
         if ( $hfc_found == 0 );


   }

   for $i ( 0 .. 2 )
   {
      $hfcc[$i] /= $nFrames;
   }
   printf "FC=%.3f  SO=%.3f  TOT=%.3f\n", $hfcc[0], $hfcc[1], $hfcc[2];


#  matrix multiplication
   sub mmult
   {
      die "Incorrect number of elements in mmult!\n"
         if @_!=18;
      my ($i,$j,$k);
      my (@a,@b,@c);
      for $i ( 0 .. 8 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+9];
      }
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $c[$i*3+$j] = 0.0;
            for $k ( 0 .. 2 )
            {
               $c[$i*3+$j] += $a[$i*3+$k] * $b[$k*3+$j];
            }
         }
      }
      return (@c);
   }

#  matrix transpose
   sub mT
   {
      my ($i,$j);
      my @a;
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $a[$j*3+$i] = $_[$i*3+$j];
         }
      }
      return (@a);
   }

#  printf matrix
   sub mprint
   {
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            printf "%8.2f", $_[$i*3+$j];
         }
         printf "\n";
      }
   }

   sub out_p
   {
      my ($i);
      my (@a,@b,@c);
      for $i ( 0 .. 2 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+3];
      }
      $c[0] = $a[1]*$b[2] - $a[2]*$b[1];
      $c[1] = $a[2]*$b[0] - $a[0]*$b[2];
      $c[2] = $a[0]*$b[1] - $a[1]*$b[0];
      return (@c);
   }


