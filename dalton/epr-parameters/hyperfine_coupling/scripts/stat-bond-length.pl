#!/usr/bin/perl

   use strict;
   use warnings;
   use Math::Trig;

   my $bin;
   my $minbin=10000;
   my $maxbin=0;
   my $maxhisto=0;
   my $delta=0.015;

   my (@histo,@r_no);

   for $bin ( $maxbin .. $minbin )
   {
      $histo[$bin] = 0.0 unless defined $histo[$bin];
   }

   my ($file, $frame, $hfc_found, $i, $j, $k, $aii, $aji);
   my (@hfc, @pa, @new_hfc, @rot_25);

#  get filenames
   my @outfiles=`ls *.hfc.out`;
   my $nFiles=@outfiles;
   my $nFrames=@outfiles;
   for $file ( 1 .. @outfiles )
   {
      chomp($outfiles[$file-1]);
      $outfiles[$file-1] =~ s/W_//;
      $outfiles[$file-1] =~ s/\.qmmm\.hfc\.out//;
   }

   my $aver_tilt = 0.0;

   my $hfcc_before = 0.0;
   my $hfcc_after = 0.0;

   for $file ( 1 .. $nFiles )
   {
      $frame = $outfiles[$file-1];

#     read coordinates and compute N-O vector
      my (@a,@x,@y,@z);
      my $atom = 0;
      my $mol_file="W_".$frame."_mol.inp";
      open MOL,"$mol_file" or die "Cannot open file $mol_file !\n";
      while ( <MOL> )
      {
         if ( /^C / or /^H / or /^O / or /^N / or /^S / )
         {
            $atom++;
            $a[$atom] = (split)[0];
            $x[$atom] = (split)[1];
            $y[$atom] = (split)[2];
            $z[$atom] = (split)[3];
         }
      }
      close MOL;
      die "Incorrect number of atoms!\n" unless $atom==71;

      $r_no[$frame]=sqrt( ($x[60]-$x[66])**2 
                        + ($y[60]-$y[66])**2 
                        + ($z[60]-$z[66])**2 );

      $bin = int( $r_no[$frame]/$delta + 0.5 );
      $minbin = $bin if $bin<$minbin;
      $maxbin = $bin if $bin>$maxbin;
      $histo[$bin]++;

      $aver_tilt += $r_no[$frame];
      #printf "%d  %.6f\n", $frame, $r_no[$frame];
   }

   printf "=============================\n";
   for $frame ( 1 .. $nFrames )
   {
      printf "%d  %.6f\n", $frame, $r_no[$frame];
   }
   printf "=============================\n";
   for $bin ( $minbin .. $maxbin )
   {
      printf "%.6f  %.1f\n", ($bin-0.5)*$delta, 0.0;
      printf "%.6f  %.1f\n", ($bin-0.5)*$delta, $histo[$bin];
      printf "%.6f  %.1f\n", ($bin+0.5)*$delta, $histo[$bin];
      printf "%.6f  %.1f\n", ($bin+0.5)*$delta, 0.0;
   }
   printf "=============================\n";
   printf "%.6f\n", $aver_tilt/$nFrames;
   printf "=============================\n";

#  matrix multiplication
   sub mmult
   {
      die "Incorrect number of elements in mmult!\n"
         if @_!=18;
      my ($i,$j,$k);
      my (@a,@b,@c);
      for $i ( 0 .. 8 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+9];
      }
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $c[$i*3+$j] = 0.0;
            for $k ( 0 .. 2 )
            {
               $c[$i*3+$j] += $a[$i*3+$k] * $b[$k*3+$j];
            }
         }
      }
      return (@c);
   }

#  matrix transpose
   sub mT
   {
      my ($i,$j);
      my @a;
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $a[$j*3+$i] = $_[$i*3+$j];
         }
      }
      return (@a);
   }

#  print matrix
   sub mprint
   {
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            printf "%8.2f", $_[$i*3+$j];
         }
         printf "\n";
      }
   }
