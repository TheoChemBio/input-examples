#!/usr/bin/perl

   use strict;
   use warnings;
   use Math::Trig;

   my ($file, $frame, $g_shift_found, $i, $j, $k, $aii, $aji);
   my (@g_shift, @pa, @new_g_shift, @rot_25);

#  get filenames
   my @outfiles=`ls *.gtensor.out`;
   my $nFiles=@outfiles;
   my $nFrames=@outfiles;
   for $file ( 1 .. @outfiles )
   {
      chomp($outfiles[$file-1]);
      $outfiles[$file-1] =~ s/W_//;
      $outfiles[$file-1] =~ s/\.qmmm\.gtensor\.out//;
   }

   my @aver_g_shift;
   for $i ( 0 .. 8 )
   {
      $aver_g_shift[$i] = 0.0;
   }

   my $g_shift_iso_before = 0.0;
   my $g_shift_iso_after = 0.0;

   for $file ( 1 .. $nFiles )
   {
      $frame = $outfiles[$file-1];
#     printf "\nFrame $frame\n";

#     read g-shift (principal axes and values) from output
      $g_shift_found = 0;
      open FL,"W_$frame.qmmm.gtensor.out"
         or die "Cannot open file W_$frame.qmmm.gtensor.out !\n";
      while ( <FL> )
      {

#@G           xx     yy     zz     xy     yx     xz     zx     yz     zy
#@G RMC     -272.  -272.  -272.     0.     0.     0.     0.     0.     0.
#@G GC1     1953.  3035.  3360.  -900. -2264.  -945. -1732.  -991.  -737.
#@G OZ-SO1  7505.   334.  5071.  7545.  5647.  2725.  1484.  2641.  2978.
#@G OZ-SO2 -2390.  1766. -1734. -4533. -2789. -1065.   -33. -1051. -1434.
#@G Total   6795.  4863.  6426.  2112.   595.   715.  -281.   599.   806.

         if ( /^\@G RMC/ )    { @_=(split); $g_shift[0]=($_[2]+$_[3]+$_[4])/3.0; }
         if ( /^\@G GC1/ )    { @_=(split); $g_shift[1]=($_[2]+$_[3]+$_[4])/3.0; }
         if ( /^\@G OZ-SO1/ ) { @_=(split); $g_shift[2]=($_[2]+$_[3]+$_[4])/3.0; }
         if ( /^\@G OZ-SO2/ ) { @_=(split); $g_shift[3]=($_[2]+$_[3]+$_[4])/3.0; }
         if ( /^\@G Total/ )  { @_=(split); $g_shift[4]=($_[2]+$_[3]+$_[4])/3.0; }

      }
      close FL;

      printf "%d%10d%10d%10d%10d%10d\n", $frame, 
         $g_shift[0],$g_shift[1],$g_shift[2],$g_shift[3],$g_shift[4];

   }

#  matrix multiplication
   sub mmult
   {
      die "Incorrect number of elements in mmult!\n"
         if @_!=18;
      my ($i,$j,$k);
      my (@a,@b,@c);
      for $i ( 0 .. 8 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+9];
      }
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $c[$i*3+$j] = 0.0;
            for $k ( 0 .. 2 )
            {
               $c[$i*3+$j] += $a[$i*3+$k] * $b[$k*3+$j];
            }
         }
      }
      return (@c);
   }

#  matrix transpose
   sub mT
   {
      my ($i,$j);
      my @a;
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            $a[$j*3+$i] = $_[$i*3+$j];
         }
      }
      return (@a);
   }

#  print matrix
   sub mprint
   {
      for $i ( 0 .. 2 )
      {
         for $j ( 0 .. 2 )
         {
            printf "%10.2f", $_[$i*3+$j];
         }
         printf "\n";
      }
   }

#  outer product
   sub out_p
   {
      my ($i);
      my (@a,@b,@c);
      for $i ( 0 .. 2 )
      {
         $a[$i] = $_[$i];
         $b[$i] = $_[$i+3];
      }
      $c[0] = $a[1]*$b[2] - $a[2]*$b[1];
      $c[1] = $a[2]*$b[0] - $a[0]*$b[2];
      $c[2] = $a[0]*$b[1] - $a[1]*$b[0];
      return (@c);
   }
