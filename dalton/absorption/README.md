[https://pubs.acs.org/doi/abs/10.1021/ct500579n](https://pubs.acs.org/doi/abs/10.1021/ct500579n)

QMCMM settings under "\*\*DALTON" section

+ QMCMM with nanoparticle only

  ```
  *QMNPMM
  .QMNP
  .DAMPED
  .PRINT
  15
  ```

+ QMCMM with nanoparticle and solvent

  ```
  *QMNPMM
  .QMNPMM
  .DAMPED
  .PRINT
  15
  ```
